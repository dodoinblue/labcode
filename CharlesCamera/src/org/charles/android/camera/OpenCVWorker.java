package org.charles.android.camera;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import java.util.List;

public class OpenCVWorker{
    public static final String IMAGE_PATH = "/sdcard/opencvimage.jpg";
    static boolean mLibLoadedOK = false;
    private final Context mContext;

    public OpenCVWorker(Context context) {
        log("OpenCVWorkder constructor");
        mContext = context;
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, mContext, new OpenCVLoaderCallback(mContext));
    }

    public void processImage() {
        Mat image = Highgui.imread(IMAGE_PATH);
        Mat gray = new Mat();
        Mat blur = new Mat();
        Imgproc.cvtColor(image, gray, Imgproc.COLOR_BGR2GRAY);
        Imgproc.GaussianBlur(gray, blur, new Size(1280, 720), 1000);
        Mat thresh = new Mat();
        Imgproc.threshold(blur, thresh, 120, 255, Imgproc.THRESH_BINARY);
//        List<MatOfPoint> list = new List<MatOfPoint>();
        Mat contours = new Mat();
//        Imgproc.findContours(thresh, list, contours, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
    }

    static void log(String s) {
        Log.i("Charles_TAG", s);
    }

    /**
     * This class will receive a callback once the OpenCV library is loaded.
     */
    private static final class OpenCVLoaderCallback extends BaseLoaderCallback {
        private Context mContext;

        public OpenCVLoaderCallback(Context context) {
            super(context);
            mContext = context;
        }

        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                    mLibLoadedOK = true;
                    Toast.makeText(mContext, "LibLoaded", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    super.onManagerConnected(status);
                    Toast.makeText(mContext, "Lib NOT Load", Toast.LENGTH_SHORT).show();
                    break;
            }
        }

    }
}
