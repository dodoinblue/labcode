package org.charles.android.labapp2;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by 28849105 on 8/5/13.
 */
public class ImageProcessor {
    Mat image;

    public ImageProcessor(Context context) {
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, context, new OpenCVLoaderCallback(context));

    }

    public void loadImage(String mImagePath) {
        image = new Mat();
        image = Highgui.imread(mImagePath);
    }

    public void processImage() {
        Imgproc.cvtColor(image, image, Imgproc.COLOR_BGR2GRAY);
        Imgproc.GaussianBlur(image, image, new Size(1, 1), 1000);
        Imgproc.threshold(image, image, 120, 255, Imgproc.THRESH_BINARY);

        List<MatOfPoint> contours = new List<MatOfPoint>() {
            @Override
            public void add(int i, MatOfPoint matOfPoint) {

            }

            @Override
            public boolean add(MatOfPoint matOfPoint) {
                return false;
            }

            @Override
            public boolean addAll(int i, Collection<? extends MatOfPoint> matOfPoints) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends MatOfPoint> matOfPoints) {
                return false;
            }

            @Override
            public void clear() {

            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> objects) {
                return false;
            }

            @Override
            public MatOfPoint get(int i) {
                return null;
            }

            @Override
            public int indexOf(Object o) {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public Iterator<MatOfPoint> iterator() {
                return null;
            }

            @Override
            public int lastIndexOf(Object o) {
                return 0;
            }

            @Override
            public ListIterator<MatOfPoint> listIterator() {
                return null;
            }

            @Override
            public ListIterator<MatOfPoint> listIterator(int i) {
                return null;
            }

            @Override
            public MatOfPoint remove(int i) {
                return null;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> objects) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> objects) {
                return false;
            }

            @Override
            public MatOfPoint set(int i, MatOfPoint matOfPoint) {
                return null;
            }

            @Override
            public int size() {
                return 0;
            }

            @Override
            public List<MatOfPoint> subList(int i, int i2) {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] ts) {
                return null;
            }
        };
        Imgproc.findContours(image, contours, new Mat(), Imgproc.RETR_TREE,Imgproc.CHAIN_APPROX_SIMPLE);

         try{
            for(MatOfPoint card : contours) {
    //            Imgproc.arcLength(card, true);
                for(Point p : card.toList()) {
                    log("Point: " + p.x + ", " + p.y);
                }
                log("=============end of contours=============");
    //        }
            }
         } catch(Exception e) {
//             log(e.getMessage());
         }
    }

    public static void log(String s){
        Log.i("Charles_TAG", s);
    }
    public void writeImage(String resultPath) {
        Highgui.imwrite(resultPath, image);
    }

    private class OpenCVLoaderCallback extends BaseLoaderCallback {
        Context mContext;
        public OpenCVLoaderCallback(Context AppContext) {
            super(AppContext);
            mContext = AppContext;
        }

        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                    Toast.makeText(mContext, "LibLoaded", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    super.onManagerConnected(status);
                    Toast.makeText(mContext, "Lib NOT Load", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }
}
