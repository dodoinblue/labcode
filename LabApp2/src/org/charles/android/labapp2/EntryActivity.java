package org.charles.android.labapp2;

import org.charles.android.labapp2.activities.Apple;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class EntryActivity extends Activity {
	
	private static String[] ACTIVITIES = {"Apple", "Avocado", "Banana",
			"Blueberry", "Coconut", "Durian", "Guava", "Kiwifruit",
			"Jackfruit", "Mango", "Olive", "Pear", "Sugar-apple"};
	
	private ListView mListView;
	private ArrayAdapter<String> mListAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.entry_layout);
		
		mListView = (ListView) this.findViewById(R.id.activity_list);
		mListAdapter = new ArrayAdapter<String>(this, R.layout.simple_list_item, R.id.title, ACTIVITIES);
		
		mListView.setAdapter(mListAdapter);
		
		mListView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {

				String title = ((TextView) arg1.findViewById(R.id.title)).getText().toString();
				
				Log.i("Charles_TAG", "sending intent");
				Intent i = new Intent();
				i.setClass(EntryActivity.this, Apple.class);
				i.setClassName("org.charles.android.labapp2", "org.charles.android.labapp2.activities." + title);
				try{
					startActivity(i);
				} catch (Exception e) {
					Log.i("Charles_TAG", e.toString());
					Toast.makeText(EntryActivity.this, "Activity " + title + " not found", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.entry_layout, menu);
		return true;
	}

}
