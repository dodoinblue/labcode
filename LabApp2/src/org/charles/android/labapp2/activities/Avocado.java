package org.charles.android.labapp2.activities;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import org.charles.android.labapp2.ImageProcessor;
import org.charles.android.labapp2.R;

/**
 * Created by 28849105 on 8/5/13.
 */
public class Avocado extends Activity {
    private static final int SELECT_IMAGE = 100;
    private static final String RESULTPATH = "/sdcard/processed_image.jpg";
    private Button mButtonLoad;
    private Button mButtonClear;
    private Button mButtonProcess;
    private ImageView mImage;
    private String mImagePath = "";
    private ImageProcessor mProcessor;
    private int mImageHeight;
    private int mImageWidth;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_view);
        mButtonLoad = (Button) findViewById(R.id.load);
        mButtonProcess = (Button) findViewById(R.id.process);
        mButtonClear = (Button) findViewById(R.id.clear);
        mImage = (ImageView) findViewById(R.id.image);

        mProcessor = new ImageProcessor(this);

        mButtonLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                log("start activity for result");
                startActivityForResult(i, SELECT_IMAGE);
            }
        });

        mButtonProcess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                processImage();
            }
        });

        mButtonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setImage(RESULTPATH);
            }
        });
    }

    private void processImage() {
        log("stamp processiamge");
        mProcessor.loadImage(mImagePath);
//        mProcessor.processImage(mImageWidth, mImageHeight);
        mProcessor.processImage();
        mProcessor.writeImage(RESULTPATH);
//        setImage(RESULTPATH);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        log("onActivityResult");

        if(resultCode == Activity.RESULT_OK){

            if(requestCode == SELECT_IMAGE) {
                Uri imageUri = intent.getData();
                setImage(imageUri);
            }
        }
    }

    private void setImage(Uri imageUri) {
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        Cursor cursor = getContentResolver().query(
                imageUri, filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String filePath = cursor.getString(columnIndex);
        cursor.close();

        setImage(filePath);
    }

    private void setImage(String filePath) {
        log("Loading image from path: " + filePath);
        mImagePath = filePath;
        Bitmap bitImage = BitmapFactory.decodeFile(filePath);
//        mImageWidth = bitImage.getWidth();
//        mImageHeight = bitImage.getHeight();
        mImage.setImageBitmap(bitImage);
    }

    public static void log(String s) {
        Log.i("Charles_TAG", s);
    }
}