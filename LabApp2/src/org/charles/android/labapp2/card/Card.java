package org.charles.android.labapp2.card;

public class Card {
	
	private String mFrontString;
	private String mBackString;
	private int mAttemptCount;
	private boolean mRemembered;
	
	public String getFrontString() {
		return mFrontString;
	}
	
	public void setFrontString(String mFrontString) {
		this.mFrontString = mFrontString;
	}
	
	public String getBackString() {
		return mBackString;
	}
	
	public void setBackString(String mBackString) {
		this.mBackString = mBackString;
	}

	public int getAttemptCount() {
		return mAttemptCount;
	}

	public void setAttemptCount(int mAttemptCount) {
		this.mAttemptCount = mAttemptCount;
	}

	public boolean isRemembered() {
		return mRemembered;
	}

	public void setRemembered(boolean mRemembered) {
		this.mRemembered = mRemembered;
	}
	
	public Card(String front, String back) {
		setFrontString(front);
		setBackString(back);
	}
}
