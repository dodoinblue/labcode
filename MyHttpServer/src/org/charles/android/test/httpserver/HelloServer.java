package org.charles.android.test.httpserver;

import android.content.Context;
import android.util.Log;

import java.io.*;
import java.util.*;

/**
 * An example of subclassing NanoHTTPD to make a custom HTTP server.
 */
public class HelloServer extends NanoHTTPD
{
    public HelloServer(Context context) throws IOException
    {
        super(5301, context.getDir("testHttpRoot", 0));
    }

    public Response serve(String uri, String method, Properties header, Properties parms,
            Properties files) {
        Log.i("Charles_TAG", method + " '" + uri + "' ");
        String msg = "";

        String _header = "<p> Header: " + header.toString() + " <br>:)</p>";
        String _parms = "<p> Parms: " + parms.toString() + " <br>:)</p>";
        Log.i("Charles_TAG", _header);
        Log.i("Charles_TAG", _parms);

        Response res = new NanoHTTPD.Response(HTTP_OK, MIME_HTML, msg);
//        res.addHeader("Cache-Control", "private");
//        res.addHeader("Allow", "OPTIONS,POST");
//        res.addHeader("Server", "Microsoft-IIS/7.0");
//        res.addHeader("MS-Server-ActiveSync", "14.00.0536.000");
//        res.addHeader("MS-ASProtocolVersions", "2.0,2.1,2.5,12.0,12.1,14.0");
//        res.addHeader("MS-ASProtocolCommands", "Sync,SendMail,SmartForward,SmartReply,GetAttachment,GetHierarchy,"
//                + "CreateCollection,DeleteCollection,MoveCollection,FolderSync,FolderCreate,FolderDelete,FolderUpdate,MoveItems,GetItemEstimate,MeetingResponse,Search,"
//                + "Settings,Ping,ItemOperations,Provision,ResolveRecipients,ValidateCert");
//        res.addHeader("Public", "OPTIONS,POST");
//        res.addHeader("X-AspNet-Version", "2.0.50727");
//        res.addHeader("X-Powered-By", "ASP.NET");
//        res.addHeader("Date", "Thu, 12 Mar 2009 20:03:29 GMT");
//        res.addHeader("Content-Length", "0");
        return res;
    }
}
