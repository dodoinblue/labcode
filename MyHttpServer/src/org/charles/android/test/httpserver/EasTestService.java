package org.charles.android.test.httpserver;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.io.IOException;

public class EasTestService extends Service {

    public static final int SERVICE_PORT = 5301;

    private NanoHTTPD mServer;

    @Override
    public IBinder onBind(Intent arg0) {
        log("onBind");
        return null;
    }

    @Override
    public void onRebind(Intent intent) {
        log("onRebind");
        super.onRebind(intent);
    }

    private void log(String s) {
        Log.i("Charles_TAG", this.getClass().getSimpleName() + " :: " + s);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        log("onStartCommand");
        super.onStartCommand(intent, flags, startId);
        try {
//            mServer = new EasHttpServer(SERVICE_PORT, this.getDir("testHttpRoot", 0));
            mServer = new HelloServer(this);
        } catch (IOException ioe) {
            log("Couldn't start server:\n" + ioe);
        }
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        log("onDestory");
        super.onDestroy();
        if (mServer != null) {
            log("Killing http server");
            mServer.stop();
        }
    }

}
