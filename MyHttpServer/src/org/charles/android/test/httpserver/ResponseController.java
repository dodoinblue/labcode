package org.charles.android.test.httpserver;

import org.charles.android.test.httpserver.NanoHTTPD.Response;

import android.util.Log;

public class ResponseController {

    public static final int STATE_WAIT_PING = 0;
    public static final int STATE_EXECUTING_TEST = 1;

    private int mState;
    private EasHttpServer mServer;
    private EasTestCase mTestCase;

    public ResponseController(EasHttpServer server) {
        mServer = server;
    }

    private void log(String s) {
        Log.i("Charles_TAG", this.getClass().getSimpleName() + " :: " + s);
    }

    public void onRequest() {
        // TODO Auto-generated method stub
    }

    public Response getResponse() {
        switch(mState) {
            case STATE_WAIT_PING:
                return mServer.new Response(NanoHTTPD.HTTP_OK, NanoHTTPD.MIME_HTML, "");
            case STATE_EXECUTING_TEST:
                log("STATE_EXECUTING_TEST");
                return generateResponseFromTestCase();
        }
        return null;
    }

    private Response generateResponseFromTestCase() {
        log("generateResponseFromTestCase");
        return mServer.new Response(NanoHTTPD.HTTP_OK, NanoHTTPD.MIME_HTML, "");
    }
}
