package org.charles.android.test.httpserver;

import org.xmlpull.v1.XmlPullParser;

import android.content.Context;
import android.util.Log;
import android.util.Xml;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.util.HashMap;

import static org.xmlpull.v1.XmlPullParser.END_DOCUMENT;
import static org.xmlpull.v1.XmlPullParser.START_TAG;


public class EasTestCase {

    private int mCurrentStep =1;
    private File mTestCaseXML;
    private HashMap<Integer, String> testSteps;
    private Context mContext;

    public EasTestCase(Context context) {
        mContext = context;
        readTestCaseFromXml();
    }

    private void readTestCaseFromXml() {
        InputStream is = null;
        try {
            is = mContext.getAssets().open("tc_account_setup/response_1.xml");
            final XmlPullParser in = Xml.newPullParser();
            in.setInput(is, null);
            int type;
            while ((type = in.next()) != END_DOCUMENT) {
                log("type: " + "" + type);
                if (type == START_TAG) {
                    final String tag = in.getName();
                    log("tag: " + tag);
                    int c = in.getAttributeCount();
                    log("Attribute count: " + "" + c);
                    for (int i = 0; i < c; i++) {
                        log("Attribute Name: " + in.getAttributeName(i));
                        log("Attribute Value: " + in.getAttributeValue(i));
                        log("Attribute Type: " + in.getAttributeType(i));
                    }
                    log("Text: " + in.getText());
                }
            }
        } catch (Exception e) {
            log("exception: " + e.toString());
        } finally {
            // in.close();
        }
    }

    private void readTestStepFromXml() {

    }

    private void log(String s) {
        Log.i("Charles_TAG", this.getClass().getSimpleName() + " :: " + s);
    }

}
