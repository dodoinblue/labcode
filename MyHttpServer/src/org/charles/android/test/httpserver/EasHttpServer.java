package org.charles.android.test.httpserver;

import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

public class EasHttpServer extends NanoHTTPD {

    ResponseController mController;

    public EasHttpServer(int port, File wwwroot) throws IOException {
        super(port, wwwroot);
        log("Constructor called");
        mController = new ResponseController(this);
    }

    private void log(String s) {
        Log.i("Charles_TAG", this.getClass().getSimpleName() + " :: " + s);
    }

    @Override
    public Response serve(String uri, String method, Properties header, Properties parms,
            Properties files) {
        log("uri: " + uri + "\n"
                + "method: " + method + "\n"
                + "header: " + header.toString() + "\n"
                + "parms: " + parms.toString() + "\n");
        mController.onRequest();
        return mController.getResponse();
    }

}
