package org.charles.android.test.httpserver;

import static org.xmlpull.v1.XmlPullParser.END_DOCUMENT;

import android.app.Activity;
import android.content.Intent;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import java.io.IOException;

import org.charles.android.test.httpserver.R;

public class MyHttpServerActivity extends Activity {

    Button mStartServer;
    Button mStopServer;
    Button mTestParser;
    private OnClickListener mOnclickListener;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mStartServer = (Button) findViewById(R.id.startservice);
        mStopServer = (Button) findViewById(R.id.stopservice);
        mTestParser = (Button) findViewById(R.id.parser);

        mOnclickListener = new OnClickListener() {

            @Override
            public void onClick(View v) {
                log("view clicked");
                if (v == mStartServer) {
                    startHttpServer();
                }
                if (v == mStopServer) {
                    stopHttpServer();
                }
                if (v == mTestParser) {
                    new EasTestCase(MyHttpServerActivity.this);
                }
            }
        };

        mStartServer.setOnClickListener(mOnclickListener);
        mStopServer.setOnClickListener(mOnclickListener);
        mTestParser.setOnClickListener(mOnclickListener);
    }

    private void log(String s) {
        Log.i("Charles_TAG", this.getClass().getSimpleName() + " :: " + s);
    }

    protected void stopHttpServer() {
        log("stopHttpServer");
        Intent intent = new Intent(this, EasTestService.class);
        stopService(intent);
    }

    protected void startHttpServer() {
        log("startHttpServer");
        Intent intent = new Intent(this, EasTestService.class);
        startService(intent);
    }
}